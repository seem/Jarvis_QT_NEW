#ifndef JARVISINTERFACE_H
#define JARVISINTERFACE_H

#include <QObject>
#include <QWidget>
#include <QString>
#include <QJsonObject>

class JarvisInterface : public QObject
{
    Q_OBJECT
public:
    enum Type{
        ALL,SPEECH,AGENT,AUDIO,RECORD,NLP,UI,OTHER
    };
    Q_ENUM(Type)
public:
    Type m_type;
public:
    virtual ~JarvisInterface(){}
    virtual void Init(QString resPath) = 0;
    virtual QWidget * GetWidget(QWidget * parent) = 0;
public slots:
    virtual void PollMessage(Type from,QJsonObject data) = 0;
signals:
    void SendData(Type to,QJsonObject data);
};

#ifdef Q_OS_ANDROID

#else
        Q_DECLARE_INTERFACE(JarvisInterface,"com.finch.jarvis")
#endif

#endif // JARVISINTERFACE_H
