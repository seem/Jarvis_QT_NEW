#ifndef MMDSHADER_H
#define MMDSHADER_H

#include <QOpenGLShaderProgram>

class MMDShader{
public:
    explicit MMDShader(QString vertexFile,QString fragFile);
    QOpenGLShaderProgram & get();
public:
    // attribute
    GLint	m_inPos = -1;
    GLint	m_inNor = -1;
    GLint	m_inUV = -1;

    // uniform
    GLint	m_uWV = -1;
    GLint	m_uWVP = -1;

    GLint	m_uAmbinet = -1;
    GLint	m_uDiffuse = -1;
    GLint	m_uSpecular = -1;
    GLint	m_uSpecularPower = -1;
    GLint	m_uAlpha = -1;

    GLint	m_uTexMode = -1;
    GLint	m_uTex = -1;
    GLint	m_uTexMulFactor = -1;
    GLint	m_uTexAddFactor = -1;

    GLint	m_uSphereTexMode = -1;
    GLint	m_uSphereTex = -1;
    GLint	m_uSphereTexMulFactor = -1;
    GLint	m_uSphereTexAddFactor = -1;

    GLint	m_uToonTexMode = -1;
    GLint	m_uToonTex = -1;
    GLint	m_uToonTexMulFactor = -1;
    GLint	m_uToonTexAddFactor = -1;

    GLint	m_uLightColor = -1;
    GLint	m_uLightDir = -1;

    GLint	m_uLightVP = -1;
private:
    QOpenGLShaderProgram mmdShader;
};
#endif // MMDSHADER_H
